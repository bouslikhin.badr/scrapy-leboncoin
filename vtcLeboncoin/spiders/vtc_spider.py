import scrapy
from scrapy_splash import SplashRequest
import logging

codePostauxTarget = ['69', '01', '38']

class QuotesSpider(scrapy.Spider):
    name = "vtc"
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36'

    def start_requests(self):
        urls = [
            'https://www.leboncoin.fr/velos/offres/rhone_alpes/',
        ]
        for url in urls:
            yield SplashRequest(url=url, callback=self.parse)

    def parse(self, response):
        for annonce in response.css('a.clearfix.trackable'):
            titre = annonce.css('section._2EDA9 p._2tubl span::text').extract_first()
            localisation = annonce.css('div._32V5I p._2qeuk::text').extract_first()
            codePostal = annonce.css('div._32V5I p._2qeuk::text').re_first(r'[0-9]*$')
            prix = annonce.css('div.lFSBn div._2OJ8g span::text').extract_first()
            for target in codePostauxTarget:
                if (codePostal.startswith(target) 
                and "enfant" not in str.lower(titre)
                and "femme" not in str.lower(titre)
                and "fille" not in str.lower(titre)
                and "chaussures" not in str.lower(titre)
                and "trottinette" not in str.lower(titre)):
                    yield {
                        'titre': titre,
                        'localisation': localisation,
                        'prix': prix,
                    }
                else:
                    pass
